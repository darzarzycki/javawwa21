package pl.sdacademy.restexample.anonymousclass;

public class Dog extends Animal {

    public Dog(String specie, int weight) {
        super(specie, weight);
    }

    public void bark() {
        System.out.println("Bark!");
    }
}
