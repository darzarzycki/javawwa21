package pl.sdacademy.restexample.anonymousclass;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal("Canis lupus", 1234) {
            @Override
            public String getSpecie() {
                return "Nadpisana metoda getSpecie";
            }
        };
        System.out.println(animal.getSpecie());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Działa!");
            }
        };
        Runnable myRunnable2 = () -> System.out.println("Działa");
    }
}
