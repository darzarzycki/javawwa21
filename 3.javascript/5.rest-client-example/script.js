let tableBody = document.querySelector("tbody");
let createPersonForm = document.querySelector("form");
let firstNameInput = document.querySelector("#firstName");
let lastNameInput = document.querySelector("#lastName");

let apiUrl = "http://localhost:8080/person";

function reloadTable() {
    fetch(apiUrl)
        .then(response => response.json())
        .then(people => {
            tableBody.innerHTML = "";
            people.forEach(person => {
                let rowElement = document.createElement("tr");
                rowElement.innerHTML = `<td>${person.firstName}</td>
                <td>${person.lastName}</td>`;
                tableBody.appendChild(rowElement);
            });
        });
}

createPersonForm.addEventListener("submit", function (event) {
    event.preventDefault();

    let person = {
        firstName: firstNameInput.value,
        lastName: lastNameInput.value
    };
    fetch(apiUrl, {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(person)
    }).then(() => reloadTable());
});

reloadTable();
