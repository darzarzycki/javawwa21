// JS.1 Stwórz skrypt, w którym zadeklarowane będą dwie zmienne. Zmiennym przypisz wartości liczbowe.
// Następnie spraw, aby w konsoli została wypisana wartość większej z nich.
let x = 3;
let y = 5;

// if (x > y) {
//     console.log(x);
// } else {
//     console.log(y);
// }
console.log(x > y ? x : y);
