let myDiv = document.querySelector("#myDiv");

myDiv.addEventListener("mouseover", function (event) {
    console.log("Najechałeś myszką na element #myDiv");
    console.log("Szczegóły zdarzenia:", event);
});

let myForm = document.querySelector("#myForm");
let myTextField = document.querySelector("#myTextField");

myForm.addEventListener("submit", function (event) {
    // Domyślnie dla formularza wysłane jest zapytanie. Nie chcemy tego robić,
    // anulujemy więc akcję domyślną.
    event.preventDefault();
    console.log("Zatwierdziłeś formularz");
    console.log("W polu tekstowym wpisałeś: " + myTextField.value);
});
